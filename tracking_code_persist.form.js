(function($, trackingCodePersist) {

  Drupal.behaviors.trackingCodePersistForm = {
    attach: function (context, settings) {
      if (settings.trackingCodePersistForm && $.isArray(settings.trackingCodePersistForm)) {
        $.each(settings.trackingCodePersistForm, function(idx, item) {
          $(context).find('#' + String.prototype.toString.call(item)).once('tracking-code-persist', function() {
            var $el = $(this);
            var parents = $el.data('tracking-code-parents');
            var data;
            // Allow optional narrowing down to a specific key.
            if (parents) {
              data = trackingCodePersist.retrieveFromCookie(false);
              data = drupal_array_get_nested_value(data, (['params']).concat(parents));
              data = (!$.isPlainObject(data) && !$.isArray(data)) ? data : JSON.stringify(data);
            }
            else {
              data = trackingCodePersist.retrieveFromCookie(true);
            }
            $el.val(data);
          });
        });
      }
    }
  };

  /**
   * Port of the Drupal core PHP function to javascript.
   *
   * @param {Array} data
   *   The data to nest into.
   * @param {Array} parents
   *   The array of parent keys to drill down into.
   */
  function drupal_array_get_nested_value(data, parents) {
    var ret = data;
    for (var i = 0; i < parents.length; i++) {
      var parent_key = parents[i];
      if (($.isArray(ret) || $.isPlainObject(ret)) && ret[parent_key]) {
        ret = ret[parent_key];
      }
      else {
        ret = undefined;
        break;
      }
    }
    return ret;
  }

})(jQuery, Drupal.trackingCodePersist);
