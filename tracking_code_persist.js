(function($) {

  Drupal.trackingCodePersist = Drupal.trackingCodePersist || {};

  if (!window.JSON) {
    return;
  }

  if (Drupal.settings.hasOwnProperty('trackingCodePersist')) {
    tracking_code_persist(Drupal.settings.trackingCodePersist);
  }
  else {
    // Need to use a behavior so we can guarantee Drupal.settings has been
    // loaded. We unset upon invocation to guarantee that we only run once.
    Drupal.behaviors.trackingCodePersist = {
      attach: function () {
        delete Drupal.behaviors.trackingCodePersist;
        if (Drupal.settings.hasOwnProperty('trackingCodePersist')) {
          tracking_code_persist(Drupal.settings.trackingCodePersist);
        }
      }
    };
  }

  /**
   * Main function - this is invoked exactly once.
   */
  function tracking_code_persist(settings) {
    var uri = new URI(location.href);
    var pathname = uri.pathname().substring(1);
    var path_match = matches_rules(pathname, settings['paths_prefix'] || [], settings['paths_exact'] || [], settings['paths_exact_exclude'] || []);
    var query_matches = hash_matches_rules_by_name(uri.query(true), settings['prefix'] || [], settings['exact'] || [], settings['exact_exclude'] || []);
    if (path_match || query_matches) {
      if (path_match) {
        path_match = [pathname];
        uri.pathname('');
      }
      if (query_matches) {
        // URI.js names array params with [] on the end. But we do
        // not do this for our cookie & server-side code for 2 reasons:
        // 1. It is unnecessary (the value being an array is clear enough)
        // 2. It does not match the Drupal/PHP approach.
        var matches_for_storage = {};
        for (var name in query_matches) {
          if (query_matches.hasOwnProperty(name)) {
            uri.removeQuery(name);
            matches_for_storage[name.replace(/\[]$/, '')] = query_matches[name];
          }
        }
      }
      add_to_cookie(path_match || null, matches_for_storage || null);
      location.href = uri.toString();
    }
  }

  /**
   * Helper which accepts a hash and returns those whose names match the rules.
   *
   * @param {Object} hash
   *   The subject hash to test.
   * @param {Array} prefix_rules
   *   Array of parameter name prefixes to match.
   * @param {Array} exact_rules
   *   Array of parameter names to match.
   * @param {Array} exact_exclude_rules
   *   Array of parameter names to exclude.
   *
   * @returns {Object}|false
   *   Key/values for the matching hash items.
   *   False if none found.
   */
  function hash_matches_rules_by_name(hash, prefix_rules, exact_rules, exact_exclude_rules) {
    var matches_found = false;
    var matches = {};
    for (var name in hash) {
      if (hash.hasOwnProperty(name)) {
        var value = hash[name];
        if (matches_rules(name, prefix_rules, exact_rules, exact_exclude_rules)) {
          matches_found = true;
          matches[name] = value;
        }
      }
    }
    return matches_found ? matches : false;
  }

  /**
   * Indicates if the given string matches the given rules.
   *
   * @param {Object} string
   *   The subject string to test.
   * @param {Array} prefix_rules
   *   Array of parameter name prefixes to match.
   * @param {Array} exact_rules
   *   Array of parameter names to match.
   * @param {Array} exact_exclude_rules
   *   Array of parameter names to exclude.
   *
   * @returns {Boolean}
   *   true  - match.
   *   false - no match.
   */
  function matches_rules (string, prefix_rules, exact_rules, exact_exclude_rules) {
    var is_match = false;
    // Include by prefix.
    for (var i = 0; i < prefix_rules.length; i++) {
      var prefix = prefix_rules[i];
      if (prefix && string.indexOf(prefix) === 0) {
        is_match = true;
      }
    }
    // Include by exact match.
    for (var i2 = 0; i2 < exact_rules.length; i2++) {
      var exact = exact_rules[i2];
      if (exact && string === exact) {
        is_match = true;
      }
    }
    // Exclude by exact match.
    if (is_match) {
      for (var i3 = 0; i3 < exact_exclude_rules.length; i3++) {
        var exact_exclude = exact_exclude_rules[i3];
        if (exact_exclude && string === exact_exclude) {
          is_match = false;
        }
      }
    }
    return is_match;
  }

  /**
   * Augment the cookie value with new data.
   *
   * Incoming paths are added to the array of existing ones.
   * Incoming params overwrite existing ones with the same key.
   */
  function add_to_cookie(new_paths, new_params) {
    var data = retrieve_from_cookie();
    // Push paths into cookie - duplicate paths accrue.
    if (new_paths) {
      new_paths = $.isArray(new_paths) ? new_paths : [new_paths];
      for (var i = 0; i < new_paths.length; i++) {
        data.paths.push(new_paths[i]);
      }
    }
    // Push params into cookie - duplicate params overwrite.
    if (new_params) {
      for (var key in new_params) {
        if (new_params.hasOwnProperty(key)) {
          // Remove trailing "[]" - the value being an array is clear enough.
          data.params[key] = new_params[key];
        }
      }
    }
    save_to_cookie(data);
  }

  /**
   * Retrieve stored tracking codes from a cookie.
   */
  function retrieve_from_cookie(return_raw) {
    var data = {
      paths: [],
      params: {}
    };
    if (return_raw) {
      data = JSON.stringify(data);
    }
    try {
      var encoded = $.cookie('tracking_code_persist');
      if (encoded) {
        var decoded = JSON.parse(encoded);
        // Basic validation of untrusted cookie.
        if (decoded && $.isPlainObject(decoded) && decoded.hasOwnProperty('paths') && decoded.hasOwnProperty('params') && $.isArray(decoded.paths) && $.isPlainObject(decoded.params)) {
          if (return_raw) {
            data = encoded;
          }
          else {
            data = decoded;
          }
        }
      }
    }
    catch (e) {}
    return data;
  }
  Drupal.trackingCodePersist.retrieveFromCookie = retrieve_from_cookie;

  /**
   * Overwrite saved tracking codes with new ones.
   */
  function save_to_cookie(data) {
    data.paths = data.paths || [];
    data.params = data.params || {};
    try {
      if ($.isPlainObject(data)) {
        $.cookie('tracking_code_persist', JSON.stringify(data), {
          path: Drupal.settings.basePath,
          expires: 365
        });
        return true;
      }
    }
    catch (e) {}
    return false;
  }

})(jQuery);
