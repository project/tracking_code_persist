<?php
/**
 * @file
 * Custom render array & form elements for Tracking code persist.
 */

/**
 * Implements hook_element_info().
 */
function tracking_code_persist_element_info() {
  $elements = array();

  $elements['tracking_code_persist'] = array(
    '#input' => TRUE,
    '#process' => array('ajax_process_form'),
    '#pre_render' => array('tracking_code_persist_pre_render_element'),
    '#theme' => 'hidden',
    '#element_validate' => array('tracking_code_persist_element_validate_decode_json'),
    // If passed, indicates the sub-portion of the query-string params to use.
    '#tracking_code_persist_parents' => array(),
    '#tracking_code_persist_raise_errors' => FALSE,
    // Pass an associative array as #default_value. You'll get one back too.
    // (We use JSON for transference / storage, but that is hidden from you).
    // Hey that rhymes..
    '#default_value' => array(
      // An indexed array of matching paths the user has visited. Will contain
      // duplicates if the user visits the same path more than once.
      'paths' => array(),
      // An associative array of matching query-string parameters with which the
      // user has loaded a page with. If a user repeats a page load with the
      // same-named parameters, the later values overwrite the earlier ones.
      'params' => array(),
    ),
  );

  return $elements;
}

/**
 * Determines the value for a tracking_code_persist form element.
 *
 * @param $element
 *   The form element whose value is being populated.
 * @param $input
 *   The incoming input to populate the form element. If this is FALSE,
 *   the element's default value should be returned.
 *
 * @return string
 *   The data that will appear in the $element_state['values'] collection
 *   for this element. Return nothing to use the default. In our case we ensure
 *   that the #value is processed as JSON.
 */
function form_type_tracking_code_persist_value($element, $input = FALSE, &$form_state) {
  if ($input !== FALSE) {
    return $input;
  }
  else {
    $default_value = NULL;
    // We allow #default_value to be a structured (non-json-encoded) value for
    // convenience. Encode it here.
    if (!is_string($element['#default_value'])) {
      if (is_array($element['#default_value']) && (isset($data['paths']) || isset($data['params']))) {
        $default_value = $element['#default_value'];
        if (!empty($element['#tracking_code_persist_parents'])) {
          $default_value = drupal_array_get_nested_value($element['#default_value'], array_merge(array('params'), $element['#tracking_code_persist_parents']));
        }
        $default_value = drupal_json_encode($default_value);
      }
    }
    // If a string was passed, verify that it was a valid JSON encoded array.
    else {
      list($is_valid, $data) = tracking_code_persist_decode_and_validate_json($element['#default_value'], $element['#tracking_code_persist_parents']);
      if ($is_valid && is_array($data) && (isset($data['paths']) || isset($data['params']) || !empty($element['#tracking_code_persist_parents']))) {
        $default_value = $element['#default_value'];
      }
    }
    if (!isset($default_value)) {
      $default_value = drupal_json_encode(array());
    }
    return $default_value;
  }
}

/**
 * Form element pre-render handler for the tracking_code_persist elements.
 *
 * @param array $element
 *   An associative array containing the properties of the element.
 *
 * @return array
 *   The processed element.
 */
function tracking_code_persist_pre_render_element($element) {
  if (isset($element['#id'])) {
    $id = $element['#attributes']['id'] = $element['#id'];
  }
  elseif (isset($element['#attributes']['id'])) {
    $id = $element['#attributes']['id'];
  }
  else {
    $id = drupal_html_id('tracking-code-persist');
  }
  $element['#id'] = $element['#attributes']['id'] = $id;
  $element['#attached']['js'][] = drupal_get_path('module', 'tracking_code_persist') . '/tracking_code_persist.form.js';
  $element['#attached']['js'][] = array(
    'data' => array(
      'trackingCodePersistForm' => array($id),
    ),
    'type' => 'setting',
  );
  if (!empty($element['#tracking_code_persist_parents'])) {
    $element['#attributes']['data-tracking-code-parents'] = drupal_json_encode($element['#tracking_code_persist_parents']);
  }
  return $element;
}

/**
 * Form API #element_validate callback to validate and decode JSON.
 *
 * @param array $element
 *   An associative array containing the properties of the element.
 */
function tracking_code_persist_element_validate_decode_json(&$element, &$form_state) {
  if ($element['#value']) {
    if (!empty($element['#tracking_code_persist_parents']) && is_scalar($element['#value'])) {
      $data = $element['#value'];
    }
    else {
      list($is_valid, $data) = tracking_code_persist_decode_and_validate_json($element['#value']);
      if (!$is_valid || !is_array($data) || !isset($data['paths']) || !isset($data['params'])) {
        $data = 'ERROR DECODING JSON ON SERVER';
        if (!empty($element['#tracking_code_persist_raise_errors'])) {
          form_error($element, t('An unexpected error occurred while saving the form data. Please try again in a different browser.'));
        }
      }
    }
    $element['#value'] = $data;
    form_set_value($element, $data, $form_state);
  }
}

/**
 * Decodes JSON and determines if it is valid.
 *
 * @param string $encoded
 *   The encoded JSON string
 * @param array $parents
 *   (Optional) Pass to use only a sub-portion of the encoded data.
 *
 * @return array
 *   With two elements:
 *     $is_valid
 *     $data
 *
 * @code
 *   list($is_valid, $data) = tracking_code_persist_decode_and_validate_json();
 */
function tracking_code_persist_decode_and_validate_json($encoded, $parents = NULL) {
  $data = json_decode($encoded, TRUE);
  if (is_array($parents) && !empty($parents)) {
    $data = drupal_array_get_nested_value($data, array_merge(array('params'), $parents));
  }
  return array(json_last_error() === JSON_ERROR_NONE, $data);
}
