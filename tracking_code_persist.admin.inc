<?php
/**
 * @file
 * Administration forms and functions for Tracking code persist module.
 */

/**
 * Form API constructor for settings form.
 *
 * @see tracking_code_persist_settings_form_submit()
 */
function tracking_code_persist_settings_form($form, &$form_state) {

  $group = 'path';
  $form[$group] = array(
    '#type' => 'fieldset',
    '#title' => t('Paths'),
    '#description' => t('On page load, if the user is at a matching path, it is stored and the user is redirected to the homepage.'),
    '#collapsable' => TRUE,
    '#collapsed' => FALSE,
  );

  $key = 'tracking_code_persist_paths_exact';
  $form[$group][$key] = array(
    '#type' => 'textarea',
    '#title' => t('Include paths (exact)'),
    '#description' => t('All paths exactly matching one of these will be matched. Put each path on a new-line.'),
    '#default_value' => implode("\n", variable_get($key, array())),
    '#element_validate' => array('tracking_code_persist_element_explode_value', 'tracking_code_persist_element_validate_trim_slashes'),
    '#field_prefix' => '<span dir="ltr">' . url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
    '#field_suffix' => '</span>&lrm;',
  );

  $key = 'tracking_code_persist_paths_prefix';
  $form[$group][$key] = array(
    '#type' => 'textarea',
    '#title' => t('Include paths by prefix'),
    '#description' => t('All paths beginning with one of these will be matched. Put each prefix on a new-line.'),
    '#default_value' => implode("\n", variable_get($key, array())),
    '#element_validate' => array('tracking_code_persist_element_explode_value', 'tracking_code_persist_element_validate_trim_slashes'),
    '#field_prefix' => '<span dir="ltr">' . url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
    '#field_suffix' => '</span>&lrm;',
  );

  $key = 'tracking_code_persist_paths_exact_exclude';
  $form[$group][$key] = array(
    '#type' => 'textarea',
    '#title' => t('Exclude paths (black-list)'),
    '#description' => t('All paths exactly matching one of these will be ignored, even if matched by the above rules. Put each path on a new-line.'),
    '#default_value' => implode("\n", variable_get($key, array())),
    '#element_validate' => array('tracking_code_persist_element_explode_value', 'tracking_code_persist_element_validate_trim_slashes'),
    '#field_prefix' => '<span dir="ltr">' . url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
    '#field_suffix' => '</span>&lrm;',
  );

  $key = 'tracking_code_persist_auto_alias';
  $form[$group][$key] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto-create URL aliases'),
    '#description' => t('Tick this box to enable auto-creation of a URL-alias for each of the <em>Include paths (exact)</em> (less the black-list) above. NOTE: The <em>Include paths by prefix</em> values will not be included. This functionality requires <em>Path</em> module to be enabled.'),
    '#default_value' => variable_get($key, FALSE),
    '#disabled' => !module_exists('path'),
  );

  $key = 'tracking_code_persist_auto_alias_target';
  $form[$group][$key] = array(
    '#type' => 'textfield',
    '#title' => t('URL aliases target'),
    '#description' => t('Specify the page these paths will be an alias for. Leave blank to indicate the front-page (recommended).'),
    '#default_value' => variable_get($key, ''),
    '#states' => array(
      'visible' => array(
        '#edit-tracking-code-persist-auto-alias' => array('checked' => TRUE),
      ),
    ),
    '#field_prefix' => '<span dir="ltr">' . url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
    '#field_suffix' => '</span>&lrm;',
    '#disabled' => !module_exists('path'),
  );

  $group = 'querystring';
  $form[$group] = array(
    '#type' => 'fieldset',
    '#title' => t('Query-string parameters'),
    '#description' => t('On page load, any matching query-string parameters are stored and stripped from the URL. The user stays at the same path.'),
    '#collapsable' => TRUE,
    '#collapsed' => FALSE,
  );

  $key = 'tracking_code_persist_codes_exact';
  $form[$group][$key] = array(
    '#type' => 'textarea',
    '#title' => t('Include query-string parameters by name'),
    '#description' => t('All query-string parameters with a name exactly matching one of these will be matched. Put each name on a new-line.'),
    '#default_value' => implode("\n", variable_get($key, array())),
    '#element_validate' => array('tracking_code_persist_element_explode_value'),
  );

  $key = 'tracking_code_persist_codes_prefix';
  $form[$group][$key] = array(
    '#type' => 'textarea',
    '#title' => t('Include query-string parameters by name-prefix'),
    '#description' => t('All query-string parameters with a name beginning with one of these prefixes will be matched. Put each prefix on a new-line.'),
    '#default_value' => implode("\n", variable_get($key, array())),
    '#element_validate' => array('tracking_code_persist_element_explode_value'),
  );

  $key = 'tracking_code_persist_codes_exact_exclude';
  $form[$group][$key] = array(
    '#type' => 'textarea',
    '#title' => t('Exclude query-string parameter names (black-list)'),
    '#description' => t('All query-string parameters with a name exactly matching one of these will be ignored, even if matched by the above rules. Put each name on a new-line.'),
    '#default_value' => implode("\n", variable_get($key, array())),
    '#element_validate' => array('tracking_code_persist_element_explode_value'),
  );

  $group = 'advanced';
  $form[$group] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsable' => TRUE,
    '#collapsed' => FALSE,
  );

  $key = 'tracking_code_persist_accelerate_js';
  $form[$group][$key] = array(
    '#title' => 'Accelerate JS <em>(experimental)</em>',
    '#type' => 'checkbox',
    '#default_value' => variable_get($key, FALSE),
    '#description' => t("This setting moves tracking_code_persist's javascript (and dependencies) to the top of the list of scripts to load in the &lt;head&gt;. This is in an attempt to reduce the lag between the user loading a landing page and being redirected. This setting may conflict with certain configurations of other advanced modules which interact with JS aggregation (e.g. <em>Advagg</em>) or certain themes."),
  );

  $form = system_settings_form($form);

  if (module_exists('path')) {
    $form['#submit'][] = 'tracking_code_persist_regenerate_url_aliases';
  }

  return $form;
}

/**
 * Form #element_validate callback for converting a textarea value to an array.
 */
function tracking_code_persist_element_explode_value(&$element, &$form_state) {
  if (!is_array($element['#value'])) {
    $element['#value'] = array_map('trim', preg_split('/(\r\n?|\n)/', $element['#value']));
    form_set_value($element, $element['#value'], $form_state);
  }
}

/**
 * Form #element_validate callback for trimming forward-slashes from the value.
 */
function tracking_code_persist_element_validate_trim_slashes(&$element, &$form_state) {
  if ($element['#value']) {
    if (is_array($element['#value'])) {
      $values = &$element['#value'];
    }
    else {
      $values = array(&$element['#value']);
    }
    foreach($values as $key => $value) {
      $values[$key] = trim($value, '/ ');
    }
    form_set_value($element, $element['#value'], $form_state);
  }
}
