<?php
/**
 * @file
 * Webform module tracking_code_persist_textfield component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_tracking_code_persist_textfield() {
  webform_component_include('textfield');
  $info = _webform_defaults_textfield();
  $info['extra']['code'] = '';
  return $info;
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_tracking_code_persist_textfield() {
  return array(
    'webform_display_tracking_code_persist_textfield' => array(
      'render element' => 'element',
      'path' => drupal_get_path('module', 'tracking_code_persist'),
      'file' => 'components/tracking_code_persist_textfield.inc',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_tracking_code_persist_textfield($component) {
  webform_component_include('textfield');
  $form = _webform_edit_textfield($component);
  unset($form['value']);
  $form['extra']['code'] = array(
    '#type' => 'textfield',
    '#title' => t('Code'),
    '#description' => t('Optionally give a specific query-string parameter name to use only. Omit to pass all query-string parameters and paths. Sub-arrays may be specified by using "][" as a separator.'),
    '#default_value' => !empty($component['extra']['code']) ? $component['extra']['code'] : '',
  );
  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_tracking_code_persist_textfield($component, $value = NULL, $filter = TRUE, $submission = NULL) {
  webform_component_include('textfield');
  $element = _webform_render_textfield($component, $value, $filter, $submission);
  if (!empty($component['extra']['code'])) {
    $element['#tracking_code_persist_parents'] = explode('][', $component['extra']['code']);
  }
  $element['#pre_render'][] = 'tracking_code_persist_pre_render_element';
  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_tracking_code_persist_textfield($component, $value, $format = 'html', $submission = array()) {
  // If a submission is rendered *after* hook_webform_submission_presave() is
  // invoked, we will have to serialize our value again for use.
  if (!empty($value[0]) && is_string($value[0]) && substr($value[0], 0, 23) === 'TRACKING_CODE_PERSIST::') {
    $value[0] = @unserialize(substr($value[0], 23));
  }
  $element = array(
    '#title' => $component['name'],
    '#tracking_code_persist_data' => isset($value[0]) ? $value[0] : array('paths' => array(), 'params' => array()),
    '#weight' => $component['weight'],
    '#format' => $format,
    '#theme' => 'webform_display_tracking_code_persist_textfield',
  );
  if ($format !== 'raw') {
    $element += array(
      '#theme_wrappers' => $format == 'text' ? array('webform_element_text') : array('webform_element'),
      '#translatable' => array('title'),
    );
  }

  return $element;
}

function theme_webform_display_tracking_code_persist_textfield($variables) {
  webform_component_include('tracking_code_persist_textfield');
  return theme_webform_display_tracking_code_persist($variables);
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_tracking_code_persist_textfield($component, $sids = array(), $single = FALSE, $join = NULL) {
  webform_component_include('textfield');
  return _webform_analysis_textfield($component, $sids, $single, $join);
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_table_tracking_code_persist_textfield($component, $value) {
  if (isset($value[0]['paths']) || isset($value[0]['params'])) {
    $value = tracking_code_persist_render_webform_value_as_raw_query($value);
  }
  else {
    $value = is_array($value[0]) ? implode(', ', $value[0]) : $value[0];
  }
  return check_plain($value);
}

/**
 * Implements _webform_action_set_component().
 */
function _webform_action_set_tracking_code_persist_textfield($component, &$element, &$form_state, $value) {
  $element['#value'] = $value;
  form_set_value($element, $value, $form_state);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_tracking_code_persist_textfield($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $export_options['header_keys'] ? $component['form_key'] : $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_tracking_code_persist_textfield($component, $export_options, $value) {
  if (isset($value[0]['paths']) || isset($value[0]['params'])) {
    $value = tracking_code_persist_render_webform_value_as_raw_query($value);
  }
  else {
    $value = is_array($value[0]) ? implode(', ', $value[0]) : $value[0];
  }
  return check_plain($value);
}
