<?php
/**
 * @file
 * Webform module tracking_code_persist component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_tracking_code_persist() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'extra' => array(
      'private' => FALSE,
      'analysis' => FALSE,
    ),
  );
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_tracking_code_persist() {
  return array(
    'webform_display_tracking_code_persist' => array(
      'render element' => 'element',
      'path' => drupal_get_path('module', 'tracking_code_persist'),
      'file' => 'components/tracking_code_persist.inc',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_tracking_code_persist($component) {
  $form = array();
  $form['extra']['code'] = array(
    '#type' => 'textfield',
    '#title' => t('Code'),
    '#description' => t('Optionally give a specific query-string parameter name to use only. Omit to pass all query-string parameters and paths. Sub-arrays may be specified by using "][" as a separator.'),
    '#default_value' => !empty($component['extra']['code']) ? $component['extra']['code'] : '',
  );
  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_tracking_code_persist($component, $value = NULL, $filter = TRUE, $submission = NULL) {
  $element = array(
    '#weight' => $component['weight'],
    '#tree' => TRUE,
  );
  $element[0] = array(
    '#type' => 'tracking_code_persist',
  );
  // Same-page conditionals depend on the wrapper around elements for getting
  // values. Wrap, but hide, the wrapper around hidden elements.
  $element['#theme_wrappers'] = array('webform_element');
  $element['#wrapper_attributes']['class'] = array();
  $element['#wrapper_attributes']['style'] = array('display: none');
  if (!empty($component['extra']['code'])) {
    $element[0]['#tracking_code_persist_parents'] = explode('][', $component['extra']['code']);
  }
  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_tracking_code_persist($component, $value, $format = 'html', $submission = array()) {
  // If a submission is rendered *after* hook_webform_submission_presave() is
  // invoked, we will have to serialize our value again for use.
  if (!empty($value[0]) && is_string($value[0]) && substr($value[0], 0, 23) === 'TRACKING_CODE_PERSIST::') {
    $value[0] = @unserialize(substr($value[0], 23));
  }
  $element = array(
    '#title' => $component['name'],
    '#tracking_code_persist_data' => isset($value[0]) ? $value[0] : array('paths' => array(), 'params' => array()),
    '#weight' => $component['weight'],
    '#format' => $format,
    '#theme' => 'webform_display_tracking_code_persist',
  );
  if ($format !== 'raw') {
    $element += array(
      '#theme_wrappers' => $format == 'text' ? array('webform_element_text') : array('webform_element'),
      '#translatable' => array('title'),
    );
  }

  return $element;
}

function theme_webform_display_tracking_code_persist($variables) {
  $element = $variables['element'];
  $component = $element['#webform_component'];
  $output = array();

  if (is_scalar($element['#tracking_code_persist_data'])) {
    $element['#tracking_code_persist_data'] = array($element['#tracking_code_persist_data']);
  }

  if (!empty($component['extra']['code'])) {
    foreach ($element['#tracking_code_persist_data'] as $value) {
      $output[] = is_array($value) ? implode(', ', $value) : $value;
    }
  }

  else {
    $data = $element['#tracking_code_persist_data'];
    $data['paths'] = array_unique($data['paths']);

    // HTML format.
    if ($element['#format'] == 'html') {
      if (!empty($data['paths']) || !empty($data['params'])) {
        $output[] = '<dl>';
        if (count($data['paths']) === 1) {
          if (!empty($data['paths']) && !empty($data['params'])) {
            $output[] = '<dt>' . t('Landing URI:') . '</dt>';
          }
          $output[] = '<dd>' . check_plain(tracking_code_persist_render_webform_data_as_raw_query($data['paths'], $data['params'])) . '</dd>';
        }
        if (!empty($data['paths']) && !empty($data['params'])) {
          $output[] = '<dt>' . format_plural(count($data['paths']), 'Landing path:', 'Landing paths (the user landed on more than one):') . '</dt>';
          foreach ((array) $data['paths'] as $path) {
            $output[] = '<dd>/' . check_plain($path) . '</dd>';
          }
          $output[] = format_plural(count($data['params']), 'Query-string parameters', 'Query-string parameters:');
          foreach ((array) $data['params'] as $name => $value) {
            $output[] = '<dt>&nbsp;&nbsp;' . check_plain($name) . (!empty($value) ? ':' : '') . '</dt>';
            if (!empty($value)) {
              if (is_scalar($value)) {
                $output[] = '<dd>&nbsp;&nbsp;' . check_plain((string) $value) . '</dd>';
              }
              else {
                foreach ((array) $value as $value_single) {
                  $output[] = '<dd>&nbsp;&nbsp;' . check_plain((string) $value_single) . '</dd>';
                }
              }
            }
          }
        }
        $output[] = '</dl>';
      }
    }

    // This special format may be passed to request a simple single-line string
    // version of our value. This is used by webform_mailchimp module. Otherwise
    // our PHP serialised value is passed, which is never desired.
    elseif ($element['#format'] === 'raw') {
      if (!empty($data['paths']) || !empty($data['params'])) {
        $data += array('paths' => array(), 'params' => array());
        $output[] = tracking_code_persist_render_webform_data_as_raw_query($data['paths'], $data['params']);
      }
    }

    // Plain text format.
    else {
      if (!empty($data['paths']) || !empty($data['params'])) {
        if (count($data['paths']) === 1) {
          if (!empty($data['paths']) && !empty($data['params'])) {
            $output[] = t('Landing URI:');
          }
          $output[] = '  ' . check_plain(tracking_code_persist_render_webform_data_as_raw_query($data['paths'], $data['params']));
        }

        if (!empty($data['paths']) && !empty($data['params'])) {
          $output[] = format_plural(count($data['paths']), 'Landing path:', 'Landing paths (the user landed on more than one):');
          foreach ((array) $data['paths'] as $path) {
            $output[] = '  /' . check_plain($path);
          }
          $output[] = format_plural(count($data['params']), 'Query-string parameters', 'Query-string parameters:');
          foreach ((array) $data['params'] as $name => $value) {
            $output[] = '  ' . check_plain($name) . (!empty($value) ? ':' : '');
            if (!empty($value)) {
              if (is_scalar($value)) {
                $output[] = '    ' . check_plain((string) $value);
              }
              else {
                foreach ((array) $value as $value_single) {
                  $output[] = '    ' . check_plain((string) $value_single);
                }
              }
            }
          }
        }
      }
    }
  }

  return implode("\n", $output);
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_tracking_code_persist($component, $sids = array(), $single = FALSE, $join = NULL) {
  webform_component_include('hidden');
  return _webform_analysis_hidden($component, $sids, $single, $join);
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_table_tracking_code_persist($component, $value) {
  if (isset($value[0]['paths']) || isset($value[0]['params'])) {
    $value = tracking_code_persist_render_webform_value_as_raw_query($value);
  }
  else {
    $value = is_array($value[0]) ? implode(', ', $value[0]) : $value[0];
  }
  return check_plain($value);
}

/**
 * Implements _webform_action_set_component().
 */
function _webform_action_set_tracking_code_persist($component, &$element, &$form_state, $value) {
  $element['#value'] = $value;
  form_set_value($element, $value, $form_state);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_tracking_code_persist($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $export_options['header_keys'] ? $component['form_key'] : $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_tracking_code_persist($component, $export_options, $value) {
  if (isset($value[0]['paths']) || isset($value[0]['params'])) {
    $value = tracking_code_persist_render_webform_value_as_raw_query($value);
  }
  else {
    $value = is_array($value[0]) ? implode(', ', $value[0]) : $value[0];
  }
  return check_plain($value);
}
