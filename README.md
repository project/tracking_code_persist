# Tracking code persist
This module detects specially-defined URL paths and/or query-string 
parameters, stores them in a cookie for later, and strips them from the
browser location bar.

Such "remembered" codes may then be injected into arbitrary form
submissions on the site. Webform integration is provided too.

## Installation

This module depends on the URI.js library. Download the latest version
from https://github.com/medialize/URI.js/releases and place it in your
site libraries directory, in a directory called "urijs", for example:

    sites/all/libraries/urijs/src/URI.min.js

The module may be configured at:

    admin/config/system/tracking_code_persist

## Webform mailchimp integration
If using the [Webform mailchimp](https://www.drupal.org/project/webform_mailchimp) module, the following, patch must be applied to fix the format of tracking code values sent to Mailchimp:

[Issue 2820445: Permit components to control the rendering of merge-field values](https://www.drupal.org/node/2820445)
